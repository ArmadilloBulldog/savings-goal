import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Chart from './Chart.js';
import MoneyValue from './MoneyValue.js';
import InputAndUpdateButton from './InputAndUpdateButton.js';
import './App.css';

class App extends Component {
  static propTypes = {
    goal: PropTypes.number,
    total: PropTypes.number,
    amount: PropTypes.number
  }
  
  constructor(props) {
    super(props);
    
    this.state = {
      goal: props.goal,
      total: props.total,
      amount: props.amount
    }
    
    this.updateAmount = this.updateAmount.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }
  
  updateAmount(newAmount) {
    this.setState((state) => {
      let newTotal = state.total + newAmount;
      
      if (newTotal > state.goal) {
        newTotal = state.goal;
      }
      return {
        total: newTotal,
        amount: newAmount
      }
    });
  }
  
  handleChange(e) {
    this.setState({
      [e.target.name]: parseFloat(e.target.value)
    });
  }
  
  render() {
    return (
      <div className="App">
        <Chart
          goal={this.state.goal}
          total={this.state.total}
          amount={this.state.amount}
        />
        
        <div className="Controls">
          <MoneyValue
            name="goal"
            label="Current Goal"
            inputValue={this.state.goal}
            handleChange={this.handleChange}
          />
          
          <MoneyValue
            name="total"
            label="Current Saving Total"
            inputValue={this.state.total}
            handleChange={this.handleChange}
          />
          
          <InputAndUpdateButton
            name="amount"
            label="Add Amount"
            inputValue={this.state.amount}
            handleChange={this.handleChange}
            updateFunction={this.updateAmount}
          />
          
          <div className="Instructions">
            Choose a goal and how much you have currently saved, then make contributions by adding additional amounts and watch yourself reach the goal.
          </div>
        </div>
      </div>
    );
  }
}

App.defaultProps = {
  goal: 0.0,
  total: 0.0,
  amount: 0.0
}

export default App;
