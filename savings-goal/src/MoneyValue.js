import React, { Component } from 'react';

class MoneyValue extends Component {
  constructor(props) {
    super(props);
    
    this.handleChange = this.props.handleChange.bind(this);
  }
  
  render() {
    return (
      <div className={this.props.name}>
        <div
          className={this.props.name + ' label'}
        >
          {this.props.label}
        </div>
        
        $
        <input
          type="number"
          size="10"
          defaultValue={this.props.inputValue}
          onBlur={this.handleChange}
          name={this.props.name}
        />
      </div>
    );
  }
}

export default MoneyValue;