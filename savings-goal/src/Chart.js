import React, { Component } from 'react';
import './Chart.css';

class Chart extends Component {
  render() {
    let progress = 100 - (this.props.total / this.props.goal * 100);
    
    return (
      <div className="Chart">
        <div className="Goal">Goal: ${this.props.goal}</div>
        <div className="Total">Total Saved: ${this.props.total}</div>
        <div className="Background">
          <div className="Progress" style={{height: progress + '%'}}>
            <div className="Stencil">
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Chart;