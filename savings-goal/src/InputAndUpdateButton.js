import React, { Component } from 'react';

class InputAndUpdateButton extends Component {
  constructor(props) {
    super(props);
    this.state = {
      inputValue: this.props.inputValue
    }
    
    this.handleChange = this.handleChange.bind(this);
    this.updateFunction = this.updateFunction.bind(this);
  }
  
  handleChange(e) {
    this.setState({
      inputValue: e.target.value
    });
  }
  
  updateFunction() {
    this.props.updateFunction(parseFloat(this.state.inputValue));
  }
  
  render() {
    return (
      <div className={this.props.name + ' Input'}>
        $
        <input
          type="number"
          size="10"
          value={this.state.inputValue}
          onChange={this.handleChange}
          name={this.props.name}
        />

        <div
          className={this.props.name + ' button'}
          onClick={this.updateFunction}
        >
          {this.props.label}
        </div>
      </div>
    );
  }
}

export default InputAndUpdateButton;